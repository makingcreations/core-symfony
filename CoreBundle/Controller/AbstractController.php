<?php

namespace MC\CoreBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * Description of AbstractController
 *
 * @author Cydrick Nonog <cydrick.dev@gmail.com>
 */
abstract class AbstractController extends Controller
{
    /**
     *
     * @return \Symfony\Component\DependencyInjection\ContainerInterface
     */
    protected function _getContainer(): \Symfony\Component\DependencyInjection\ContainerInterface
    {
        return $this->container;
    }

    /**
     * Get Router
     *
     * @return \Symfony\Bundle\FrameworkBundle\Routing\Router
     */
    protected function _getRouter(): \Symfony\Bundle\FrameworkBundle\Routing\Router
    {
        return $this->getContainer()->get('router');
    }

    /**
     * Get Session
     *
     * @return \Symfony\Component\HttpFoundation\Session\Session
     */
    protected function _getSession(): \Symfony\Component\HttpFoundation\Session\Session
    {
        return $this->getContainer()->get('session');
    }

    /**
     * Get translator
     *
     * @return \Symfony\Component\Translation\DataCollectorTranslator
     */
    protected function _getTranslator(): \Symfony\Component\Translation\DataCollectorTranslator
    {
        return $this->getContainer()->get('translator');
    }
}