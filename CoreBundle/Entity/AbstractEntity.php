<?php

namespace MC\CoreBundle\Entity;

/**
 * @author Cydrick Nonog <cydrick.dev@gmail.com>
 */
abstract class AbstractEntity
{
    protected $id;

    /**
     * Return the entity id
     * 
     * @return int The id of the entity
     */
    public function getId(): int
    {
        return $this->id;
    }
}