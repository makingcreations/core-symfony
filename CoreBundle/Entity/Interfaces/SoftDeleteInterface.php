<?php

namespace MC\CoreBundle\Entity\Interfaces;

/**
 *
 * @author Cydrick Nonog <cydrick.dev@gmail.com>
 */
interface SoftDeleteInterface
{
    /**
     * Return when it was deleted
     *
     * @return \DateTime|null The date when it was deleted, return null if not yet deleted
     */
    public function getDeletedAt();

    /**
     * Return the user who did the deletion
     *
     * @return mixed The user who deleted the entity,
     * return null if not yet deleted
     */
    public function getDeletedBy();

    /**
     * Set when the deletion happens
     *
     * @param \DateTime $deletedAt
     */
    public function setDeletedAt(\DateTime $deletedAt);

    /**
     * Set who deleted the entity
     *
     * @param \Symfony\Component\Security\Core\User\UserInterface $deletedBy
     */
    public function setDeletedBy($deletedBy);
}