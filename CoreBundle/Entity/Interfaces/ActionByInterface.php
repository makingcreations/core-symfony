<?php

namespace MC\CoreBundle\Entity\Interfaces;

/**
 * @author Cydrick Nonog <cydrick.dev@gmail.com>
 */
interface ActionByInterface
{
    /**
     * Return the creator of the entity
     *
     * @return mixed The user who created the entity
     */
    public function getCreatedBy();

    /**
     * Returns who update the entity
     *
     * @return mixed The user who updated the entity
     */
    public function getUpdatedBy();

    /**
     * Set who created the entity
     *
     * @param mixed $createdBy
     */
    public function setCreatedBy($createdBy);

    /**
     * Set who updated the entity
     *
     * @param mixed $updatedBy
     */
    public function setUpdatedBy($updatedBy);
}