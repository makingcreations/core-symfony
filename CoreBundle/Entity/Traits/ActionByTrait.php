<?php

namespace MC\CoreBundle\Entity\Traits;

/**
 *
 * @author Cydrick Nonog <cydrick.dev@gmail.com>
 */
trait ActionByTrait
{
    /**
     * @var mixed
     */
    protected $createdBy;

    /**
     * @var mixed
     */
    protected $updatedBy;

     /**
     * Return the creator of the entity
     *
     * @return mixed The user who created the entity
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * Returns who update the entity
     *
     * @return mixed The user who updated the entity
     */
    public function getUpdatedBy()
    {
        return $this->updatedBy;
    }

    /**
     * Set who created the entity
     *
     * @param mixed $createdBy
     */
    public function setCreatedBy($createdBy)
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    /**
     * Set who updated the entity
     *
     * @param mixed $updatedBy
     */
    public function setUpdatedBy($updatedBy)
    {
        $this->updatedBy = $updatedBy;

        return $this;
    }
}