<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace MC\CoreBundle\Entity\Traits;

/**
 * @author Cydrick Nonog <cydrick.dev@gmail.com>
 */
trait SoftDeleteTrait
{
    /**
     * @var \DateTime
     */
    private $deletedAt;
    
    /**
     * @var \Symfony\Component\Security\Core\User\UserInterface
     */
    private $deletedBy;

    /**
     * Return when it was deleted
     *
     * @return \DateTime|null The date when it was deleted, return null if not yet deleted
     */
    public function getDeletedAt(): \DateTime
    {
        return $this->deletedAt;
    }

    /**
     * Return the user who did the deletion
     *
     * @return \Symfony\Component\Security\Core\User\UserInterface|null The user who deleted the entity,
     * return null if not yet deleted
     */
    public function getDeletedBy()
    {
        return $this->deletedBy;
    }

    /**
     * Set when the deletion happens
     *
     * @param \DateTime $deletedAt
     *
     * @return EntityInterface
     */
    public function setDeletedAt(\DateTime $deletedAt = null)
    {
        $this->deletedAt = $deletedAt;
        
        return $this;
    }


    /**
     * Set who deleted the entity
     *
     * @param \Symfony\Component\Security\Core\User\UserInterface $deletedBy
     *
     * @return EntityInterface
     */
    public function setDeletedBy($deletedBy)
    {
        $this->deletedBy = $deletedBy;

        return $this;
    }

    public function isDeleted()
    {
        return null !== $this->deletedAt;
    }
}