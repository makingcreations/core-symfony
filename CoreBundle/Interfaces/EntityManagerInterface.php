<?php

namespace MC\CoreBundle\Manager\Interfaces;

/**
 *
 * @author cnonog
 */
interface EntityManagerInterface extends ManagerInterface
{
    public function getErrorMessages($form);
    public function getRepository();
    public function delete($entity);
    public function save($entity);
}