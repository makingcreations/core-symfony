<?php

namespace MC\CoreBundle\Interfaces;

use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

/**
 *
 * @author Cydrick Nonog <cydrick.dev@gmail.com>
 */
interface UserAwareInterface
{
    public function setTokenStorage(TokenStorageInterface $tokenStorage);
}