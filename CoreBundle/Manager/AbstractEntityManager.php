<?php

namespace MC\CoreBundle\Manager;

use Doctrine\Bundle\DoctrineBundle\Registry;
use Symfony\Component\Form\Form;
use MC\CoreBundle\Entity\Interfaces\EntityInterface;
use MC\CoreBundle\Manager\Interfaces\EntityManagerInterface;

/**
 * Description of AbstractEntityManager
 *
 * @author Cydrick Nonog <cydrick.dev@gmail.com>
 */
abstract class AbstractEntityManager implements EntityManagerInterface
{
    use \MC\CoreBundle\Traits\UserAwareTrait;
    
    protected $Doctrine;
    protected $repositoryName;


    /**
     * Contruct Manager
     * 
     * @param Registry $registry
     * @param string $repositoryName
     */
    public function __construct(Registry $Registry, string $repositoryName)
    {
        $this->Doctrine = $Registry;
        $this->repositoryName = $repositoryName;
    }

    public function delete(EntityInterface $Entity, bool $flush = false, $managerName = null)
    {
        $this->_getEntityManager($managerName)->remove($Entity);
        if ($flush) {
            $this->flush($Entity, $managerName);
        }
    }

    public function getErrorMessages(Form $Form): array
    {
        $errors = [];
        foreach ($Form->getErrors() as $key => $error) {
            $errors[$key] = $error->getMessage();
        }
        foreach ($Form as $child) {
            if (!$child->isValid()) {
                $errors[$child->getName()] = $this->getErrorMessages($child);
            }
        }
        
        return $errors;
    }

    /**
     * Get Repository
     *
     * @param string|null $managerName
     * 
     * @return \Doctrine\ORM\EntityRepository
     */
    public function getRepository($managerName = null): \Doctrine\ORM\EntityRepository
    {
        return $this->_getEntityManager($managerName)->getRepository($this->repositoryName);
    }

    public function flush(EntityInterface $Entity, $managerName = null)
    {
        $this->_getEntityManager($managerName)->flush($Entity);
    }

    public function save(EntityInterface $Entity, bool $flush = false, $managerName = null)
    {
        $this->_getEntityManager($managerName)->persist($entity);
        if ($flush) {
            $this->flush($entity, $managerName);
        }
    }

    /**
     * Get Doctrine
     * 
     * @return Registry
     */
    protected function _getDoctrine(): Registry
    {
        return $this->Doctrine;
    }

    /**
     * Get Entity Manager
     *
     * @param string|null $name
     * 
     * @return \Doctrine\ORM\EntityManagerInterface
     */
    protected function _getEntityManager($name = null): \Doctrine\ORM\EntityManagerInterface
    {
        return $this->_getDoctrine()->getManager($name);
    }
}