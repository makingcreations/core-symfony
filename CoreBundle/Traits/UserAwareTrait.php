<?php

namespace MC\CoreBundle\Traits;

use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

/**
 * Description of UserAwareTrait
 *
 * @author Cydrick Nonog <cydrick.dev@gmail.com>
 */
trait UserAwareTrait
{
    /**
     * @var TokenStorageInterface
     */
    protected $tokenStorage;

    /**
     * Set token storage
     *
     * @param TokenStorageInterface $tokenStorage
     */
    public function setTokenStorage(TokenStorageInterface $tokenStorage)
    {
        $this->tokenStorage = $tokenStorage;
    }

    /**
     * Get token storage
     * @return TokenStorageInterface
     */
    protected function _getTokenStorage(): TokenStorageInterface
    {
        return $this->tokenStorage;
    }

    /**
     * Get current user
     *
     * @return type
     * @throws \LogicException
     */
    protected function _getUser()
    {
         if (!($this->tokenStorage instanceof TokenStorageInterface)) {
            throw new \LogicException('The SecurityBundle is not registered in your application.');
        }

        $token = $this->tokenStorage->getToken();
        if (null === $token) {
            return;
        }

        if (!is_object($token->getUser())) {
            // e.g. anonymous authentication
            return;
        }

        return $token->getUser();
    }
}