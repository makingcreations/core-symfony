<?php

namespace MC\CoreBundle\EventListener;

use Doctrine\Common\Persistence\Event\LifecycleEventArgs;
use MC\CoreBundle\Interfaces\UserAwareInterface;

/**
 *
 * @author Cydrick Nonog <cydrick.dev@gmail.com>
 */
class ActionByListener implements UserAwareInterface
{
    use \MC\CoreBundle\Traits\UserAwareTrait;

    public function prePersist(LifecycleEventArgs $eventArgs)
    {
        $entity = $eventArgs->getObject();
        if ($entity instanceof \MC\CoreBundle\Entity\Interfaces\ActionByInterface) {
            $entity->setCreatedBy($this->_getUser());
        }
    }

    public function preUpdate(LifecycleEventArgs $eventArgs)
    {
        $entity = $eventArgs->getObject();
        if ($entity instanceof \MC\CoreBundle\Entity\Interfaces\ActionByInterface) {
            $entity->setUpdatedBy($this->_getUser());
        }
    }
}