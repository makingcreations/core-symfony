<?php
namespace MC\CoreBundle\EventListener;

use Doctrine\Common\Persistence\Event\LifecycleEventArgs;

/**
 * @author Cydrick Nonog <cydrick.dev@gmail.com>
 */
class TimestampListener
{
    public function prePersist(LifecycleEventArgs $eventArgs)
    {
        $entity = $eventArgs->getObject();
        if ($entity instanceof \MC\CoreBundle\Entity\Interfaces\TimestampInterface) {
            $entity->setCreatedAt(new \DateTime());
        }
    }

    public function preUpdate(LifecycleEventArgs $eventArgs)
    {
        $entity = $eventArgs->getObject();
        if ($entity instanceof \MC\CoreBundle\Entity\Interfaces\TimestampInterface) {
            $entity->setUpdatedAt(new \DateTime());
        }
    }
}