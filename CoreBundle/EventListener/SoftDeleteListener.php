<?php
namespace MC\CoreBundle\EventListener;

use Doctrine\ORM\Event\OnFlushEventArgs;
use MC\CoreBundle\Interfaces\UserAwareInterface;

/**
 * @author Cydrick Nonog <cydrick.dev@gmail.com>
 */
class SoftDeleteListener implements UserAwareInterface
{
    use \MC\CoreBundle\Traits\UserAwareTrait;

    public function onFlush(OnFlushEventArgs $eventArgs)
    {
        $em = $eventArgs->getEntityManager();
        $uow = $em->getUnitOfWork();

        foreach ($uow->getScheduledEntityDeletions() as $key => $entity) {
            if ($entity instanceof \MC\CoreBundle\Entity\Interfaces\SoftDeleteInterface) {
                $oldValue = $entity->getDeletedAt();
                if ($oldValue !== null) {
                    continue;
                }
                $date = new \DateTime();
                $user = $this->_getUser();
                $entity->setDeletedAt($date);
                $entity->setDeletedBy($user);

                $uow->persist($entity);
                $uow->propertyChanged($entity, 'deletedAt', $oldValue, $date);
                $uow->propertyChanged($entity, 'deletedBy', $oldValue, $user);
            }
        }
    }
}