<?php

namespace MC\CoreBundle\DBAL\Driver\MySql;

use Doctrine\DBAL\DBALException;
use Doctrine\DBAL\Driver\PDOMySql\Driver as PDODriver;
use Doctrine\DBAL\Platforms\MySQL57Platform as DoctrineMySQL57Platform;
use MC\CoreBundle\DBAL\Platforms\MySQL57Platform;

/**
 * Description of MySqlDriver
 *
 * @author Cydrick Nonog <cydrick.dev@gmail.com>
 */
class Driver extends PDODriver
{
    /**
     * {@inheritdoc}
     */
    public function createDatabasePlatformForVersion($version)
    {
        if ( ! preg_match('/^(?P<major>\d+)(?:\.(?P<minor>\d+)(?:\.(?P<patch>\d+))?)?/', $version, $versionParts)) {
            throw DBALException::invalidPlatformVersionSpecified(
                $version,
                '<major_version>.<minor_version>.<patch_version>'
            );
        }

        if (false !== stripos($version, 'mariadb')) {
            return $this->getDatabasePlatform();
        }

        $majorVersion = $versionParts['major'];
        $minorVersion = isset($versionParts['minor']) ? $versionParts['minor'] : 0;
        $patchVersion = isset($versionParts['patch']) ? $versionParts['patch'] : 0;
        $version      = $majorVersion . '.' . $minorVersion . '.' . $patchVersion;

        if (version_compare($version, '5.7.8', '>=')) {
            return new MySQL57Platform();
        } else {
            return new DoctrineMySQL57Platform();
        }

        return $this->getDatabasePlatform();
    }
}