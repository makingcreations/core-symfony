<?php

namespace MC\CoreBundle\DBAL\Platforms;

use Doctrine\DBAL\Platforms\MySQL57Platform as BasePlatform;

/**
 * Description of MySql57Platform
 *
 * @author Cydrick Nonog <cydrick.dev@gmail.com>
 */
class MySQL57Platform extends BasePlatform
{
    public function getJsonTypeDeclarationSQL(array $field)
    {
        return 'JSON';
    }

    public function getTinyIntTypeDeclarationSQL(array $field)
    {
        return 'TINYINT' . $this->_getCommonIntegerTypeDeclarationSQL($field);
    }
}