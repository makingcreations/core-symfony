<?php

namespace MC\UserBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * @author Cydrick Nonog <cydrick.dev@gmail.com>
 */
class MCUserBundle extends Bundle
{
}
