<?php

namespace MC\UserBundle\Repository;

/**
 * UserGroupRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 *
 * @author Cydrick Nonog <cydrick.dev@gmail.com>
 */
class UserGroupRepository extends \Doctrine\ORM\EntityRepository
{
}
