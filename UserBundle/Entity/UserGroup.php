<?php

namespace MC\UserBundle\Entity;

use Symfony\Component\Security\Core\User\UserInterface;
use MC\CoreBundle\Entity\AbstractEntity;
use MC\CoreBundle\Entity\Interfaces\EntityInterface;
use MC\CoreBundle\Entity\Interfaces\SoftDeleteInterface;
use MC\CoreBundle\Entity\Interfaces\TimestampInterface;
use MC\CoreBundle\Entity\Interfaces\ActionByInterface;
use MC\UserBundle\Entity\Interfaces\RoleInterface;

/**
 * UserGroup
 * @author Cydrick Nonog <cydrick.dev@gmail.com>
 */
class UserGroup extends AbstractEntity implements
    EntityInterface,
    ActionByInterface,
    TimestampInterface,
    RoleInterface,
    SoftDeleteInterface
{
    use \MC\CoreBundle\Entity\Traits\SoftDeleteTrait;
    use \MC\CoreBundle\Entity\Traits\TimestampTrait;
    use \MC\CoreBundle\Entity\Traits\ActionByTrait;
    
    /**
     * @var string
     */
    protected $name;

    /**
     * @var array
     */
    protected $roles;

    public function __construct(string $name)
    {
        $this->setName($name);
        $this->setRoles([]);
    }

    public function isGranted($role): bool
    {
        return array_has($this->roles, $role);
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Get roles
     *
     * @return array
     */
    public function getRoles()
    {
        return $this->roles;
    }
    
    /**
     * Set name
     *
     * @param string $name
     *
     * @return UserGroup
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Set roles
     *
     * @param array $roles
     *
     * @return UserGroup
     */
    public function setRoles($roles)
    {
        $this->roles = $roles;

        return $this;
    }
}
