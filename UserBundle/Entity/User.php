<?php

namespace MC\UserBundle\Entity;

use Symfony\Component\Security\Core\User\UserInterface;
use MC\CoreBundle\Entity\AbstractEntity;
use MC\CoreBundle\Entity\Interfaces\EntityInterface;
use MC\CoreBundle\Entity\Interfaces\SoftDeleteInterface;
use MC\CoreBundle\Entity\Interfaces\TimestampInterface;
use MC\CoreBundle\Entity\Interfaces\ActionByInterface;
use MC\UserBundle\Entity\Interfaces\RoleInterface;

/**
 * User
 * @author Cydrick Nonog <cydrick.dev@gmail.com>
 */
class User extends AbstractEntity implements
    EntityInterface,
    ActionByInterface,
    TimestampInterface,
    SoftDeleteInterface,
    UserInterface,
    RoleInterface,
    \Serializable
{
    use \MC\CoreBundle\Entity\Traits\SoftDeleteTrait;
    use \MC\CoreBundle\Entity\Traits\TimestampTrait;
    use \MC\CoreBundle\Entity\Traits\ActionByTrait;

    protected $isActive;
    protected $type;
    protected $username;
    protected $password;
    protected $roles;
    protected $preferences;
    protected $owner;
    protected $plainPassword;
    
    private $_roles;

    /**
     * @var \MC\UserBundle\Entity\UserGroup
     */
    protected $group;

    public function __construct(string $username)
    {
        $this->setUsername($username);
        $this->setRoles([]);
        $this->setPreferences([]);
        $this->setIsActive(false);
        $this->setGroup(null);
    }

    public function eraseCredentials()
    {
        $this->plainPassword = "";
    }

    public function isGranted($role)
    {
        return array_has($this->_roles, $role);
    }

    public function getIsActive(): bool
    {
        return $this->isActive;
    }

    public function getPassword(): string
    {
        return $this->password;
    }

    public function getPlainPassword(): string
    {
        return $this->plainPassword;
    }

    public function getPreference($preference, $default = null)
    {
        return array_get($this->preferences, $preference, $default);
    }

    public function getPreferences(): array
    {
        return $this->preferences;
    }

    public function getRoles(): array
    {
        return $this->_roles;
    }

    public function getSalt()
    {
        return "MC.User.Security_Asin";
    }

    public function getType()
    {
        return $this->type;
    }

    public function getUsername(): string
    {
        return $this->username;
    }

    public function setIsActive($isActive): User
    {
        $this->isActive = $isActive;

        return $this;
    }

    public function setPassword($password): User
    {
        $this->password = $password;

        return $this;
    }

    public function setPlainPassword($password): User
    {
        $this->plainPassword = $password;

        return $this;
    }

    public function setPreference($preference, $value): User
    {
        array_set($this->preferences, $preference, $value);

        return $this;
    }

    public function setPreferences($preferences): User
    {
        $this->preferences = $preferences;

        return $this;
    }

    public function setRole($role, $value): User
    {
        array_set($this->roles, $role, $value);

        return $this;
    }

    public function setRoles($roles): User
    {
        $this->roles = $roles;
        $this->_parseRoles();
        
        return $this;
    }

    public function setType($type): User
    {
        $this->type = $type;

        return $this;
    }

    public function setUsername($username): User
    {
        $this->username = $username;

        return $this;
    }

    public function serialize(): string
    {
        return serialize([
            'id'       => $this->getId(),
            'username' => $this->getUsername(),
            'type'     => $this->getType()
        ]);
    }

    public function unserialize($serialized)
    {
        list(
            $this->id,
            $this->username,
            $this->type
            ) = unserialize($serialized);
    }

    /**
     * Set group
     *
     * @param \MC\UserBundle\Entity\UserGroup $group
     *
     * @return User
     */
    public function setGroup(\MC\UserBundle\Entity\UserGroup $group = null)
    {
        $this->group = $group;

        return $this;
    }

    /**
     * Get group
     *
     * @return \MC\UserBundle\Entity\UserGroup
     */
    public function getGroup()
    {
        return $this->group;
    }

    private function _parseRoles()
    {
        $this->_roles = [];
        foreach ($this->roles as $role => $value) {
            if ($value == 1) {
                $this->_roles[] = $role;
            } elseif ($value == 2 && $this->getGroup() !== null) {
                if ($this->getGroup()->isGranted($role)) {
                    $this->_roles[] = $role;
                }
            }
        }
    }
}
