<?php

namespace MC\UserBundle\Entity\Interfaces;

/**
 * Description of RoleInterface
 *
 * @author Cydrick Nonog <cydrick.dev@gmail.com>
 */
interface RoleInterface
{
    /**
     * Check if granted to specific role
     * 
     * @param string $role
     *
     * @return bool Return true if granted, otherwise false
     */
    public function isGranted($role): bool;

    /**
     * @return array Return the all roles
     */
    public function getRoles(): array;
}