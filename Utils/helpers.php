<?php

if (! function_exists('value')) {
    /**
     * Return the default value of the given value.
     *
     * @param  mixed  $value
     * @return mixed
     */
    function value($value)
    {
        return $value instanceof Closure ? $value() : $value;
    }
}

if (function_exists('array_get')) {
    function array_get($array, $key, $default = null)
    {
        return \MC\CoreBundle\Helper\ArrayHelper::get($array, $key, $default);
    }
}

if (!function_exists('array_has')) {
    function array_has($array, $key)
    {
        return \MC\CoreBundle\Helper\ArrayHelper::has($array, $key);
    }
}

if (!function_exists('array_set')) {
    function array_set(&$array, $key, $value)
    {
        return \MC\CoreBundle\Helper\ArrayHelper::set($array, $key, $value);
    }
}

if (!function_exists('array_dot')) {
    function array_dot($array, $delimeter = '.', $prepend = '', $notLast = false)
    {
        return \MC\CoreBundle\Helper\ArrayHelper::dot($array, $delimeter, $prepend, $notLast);
    }
}

if (!function_exists('array_forget')) {
    function array_forget(&$array, $keys)
    {
        return \MC\CoreBundle\Helper\ArrayHelper::forget($array, $keys);
    }
}